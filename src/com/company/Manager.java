package com.company;

public class Manager {
    Secretary secretary = new Secretary();

    public void doJob(Boss boss) {
        secretary.doJob(this);
    }
}
